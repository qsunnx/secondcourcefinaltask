//
//  PostCell.swift
//  Course2FinalTask
//
//  Created by Qsunnx on 31/10/2018.
//  Copyright © 2018 e-Legion. All rights reserved.
//

import UIKit


protocol PostActionDelegate : class {
    func likePost(postCell: PostCell)
}

class PostCell: UITableViewCell {
    
    @IBOutlet var userAvatar: UIImageView!
    @IBOutlet var userName: UILabel!
    @IBOutlet var postDate: UILabel!
    @IBOutlet var postImage: UIImageView!
    @IBOutlet var likesCountLabel: UILabel!
    @IBOutlet var likeButton: UIButton!
    @IBOutlet var postDescription: UILabel!
    
    weak var delegate: PostActionDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func likeButtonPressed(_ sender: UIButton) {
        delegate?.likePost(postCell: self)
    }
    
    func updateLikesCount(likedByCount: Int) {
        self.likesCountLabel.text = "Likes: \(likedByCount)"
    }
    
}
