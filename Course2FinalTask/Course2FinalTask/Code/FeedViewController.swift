//
//  FeedViewController.swift
//  Course2FinalTask
//
//  Created by Qsunnx on 28/10/2018.
//  Copyright © 2018 e-Legion. All rights reserved.
//

import UIKit
import Foundation
import DataProvider

class FeedViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, PostActionDelegate {
    
    @IBOutlet private var postsTableView: UITableView!
    
    private var posts: [Post]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.posts = DataProviders.shared.postsDataProvider.feed();
        
        postsTableView.delegate = self;
        postsTableView.dataSource = self;
    }
    
    // MARK: -TableView dataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let posts = posts {
            return posts.count;
        } else {
            return 0;
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell") as! PostCell
        
        cell.delegate = self;
        
        if let currentPost = posts?[indexPath.row] {
            cell.userAvatar.image       = currentPost.authorAvatar
            cell.userName.text          = currentPost.authorUsername
            cell.postImage.image        = currentPost.image
            cell.postDescription.text   = currentPost.description
            cell.updateLikesCount(likedByCount: currentPost.likedByCount)
        }
        
        return cell
    }
    
    // MARK: - PostActionDelegate
    
    func likePost(postCell: PostCell) {
        if let postIndex = postsTableView.indexPath(for: postCell) {
            var post = posts?[postIndex.row]
            if post != nil {
                post!.likedByCount += 1
                postCell.updateLikesCount(likedByCount: post!.likedByCount)
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
